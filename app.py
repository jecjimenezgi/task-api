# app.py
import json
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

VALIDATION_SCHEME = {
   "type" : "object",
     "properties" : {
         "id": {"type" : "number"},
         "task" : {"type" : "string"},
         "student" : {"type" : "string"},
         "state": {"type" : "boolean"},
     },
    "required": ["task","student"],
    "propertyNames": {
        "enum": ["id", "task", "student","state"] 
     },
 }

def validate_task_error(task):
    try:
        validate(instance=task, schema=VALIDATION_SCHEME)
    except ValidationError as e:
        return str(e)


def read_json():
    with open("db.json","r",encoding='utf-8') as f:
        tasks = json.load(f)
    return tasks

def write_json(tasks):
    with open("db.json", "w",encoding='utf-8') as f:
        json.dump(tasks,f,ensure_ascii=False, indent=4)


def _find_next_id():
    tasks = read_json()
    if len(tasks) == 0:
        return 1
    return max(task["id"] for task in tasks) + 1

@app.get("/tasks")
def get_tasks():
    tasks = read_json()
    return jsonify(tasks)

@app.post("/tasks")
def add_task():
    tasks = read_json()
    if request.is_json:
        task = request.get_json()
        task["id"] = _find_next_id()
        task["state"] = False
        tasks.append(task)
        e = validate_task_error(task)
        if e: 
            return e, 404
        write_json(tasks)
        return task, 201
    return {"error": "Request must be JSON"}, 415

@app.put("/tasks/<int:id>")
def edit_task(id):
    tasks = read_json()
    if request.is_json:
        task = request.get_json()
        for c in tasks:
            if c["id"] == id:
                c.update(task)
                e = validate_task_error(task)
                if e: 
                    return e, 404
                write_json(tasks)
                return c, 201
        return {"error": "task not found"}, 404

    return {"error": "Request must be JSON"}, 415

@app.delete("/tasks/<int:id>")
def delete_task(id):
    tasks = read_json()
    for c in tasks:
        if c["id"] == id:
            tasks.remove(c)
            write_json(tasks)
            return c, 201
    return {"error": "task not found"}, 404

