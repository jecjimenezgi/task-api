# Regular Operations

* GET /objects/ Gets all Objects
    respond: 
    ´´´
        [
        {
            "id": 1,
            "state": false,
            "student": "jean",
            "task": "cambio"
        }
        ]
    ´´´

* POST /objects Adds a new Object
    payload:    
    ´´´
        {
            "student": "jean",
            "task": "cambio"
        }
    ´´´
* PUT /objects/ID Adds an Object with specified ID, Updates an Object

* DELETE /object/ID Deletes the object with specified ID
